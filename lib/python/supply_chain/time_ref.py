from dateutil.parser import parse as parse_date
import dateutil
import os.path
from datetime import datetime, timedelta
import json
json_data = None

def get_json_data(json_data):
    if json_data is None:
        demo_path = os.path.join(os.path.dirname(__file__), 'demo_time.json')
        json_data = json.load(file(demo_path))
    return json_data

def time_start():
    timeconfig = get_json_data(json_data)
    return timeconfig["start_time"]

def monthly_end():
    timeconfig = get_json_data(json_data)
    return timeconfig["monthly_end"]

def daily_start():
    timeconfig = get_json_data(json_data)
    return timeconfig["daily_start"]

def daily_end():
    timeconfig = get_json_data(json_data)
    return timeconfig["daily_end"]

def hourly_start():
    timeconfig = get_json_data(json_data)
    return timeconfig["hourly_start"]

def hourly_end():
    timeconfig = get_json_data(json_data)
    return timeconfig["hourly_end"]

def sparkline_lookback_secs():
    return 7200

EPOCH_TIME = datetime(1970,1,1,tzinfo=dateutil.tz.tzutc())
def epoch_time(ts):
    return (ts - EPOCH_TIME).total_seconds()

#NOTE: This is central time
START_TIME = parse_date('2013-11-01 00:54:32-05')
#also = 2013-11-01 05:54:32 GMT
#     = 2013-10-31 22:54:32 (-7/ CA time)
START_TIME_EPOCH = epoch_time(START_TIME)
