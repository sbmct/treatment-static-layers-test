from flask import Flask
from flask import render_template
import flask
import re
app = Flask(__name__)

@app.route('/<ul>/<ur>/<lower>')
def main(ur=None,ul=None,lower=None):
    if len(ul)==2:
        a = "20px"
        f = "2px"
        e = "0px"
    elif len(ul)==3:
        a = "14px"
        f = "6px"
        e = "-4px"
    else:
        e = "1px"
        f = "2px"
        a = "27px"
    if len(ur)==2:
        b = "20px"
        f = "2px"
    elif len(ur)==3:
        b = "14px"
        f = "6px"
    else:
        b = "27px"
        f = "2px"
    
    if len(lower)==2:
        c = "20px"
        d = "-8px"
    elif len(lower)==3:
        d = "0px"
        c = "14px"
    else:
        d = "-8px"
        c = "27px"
    return render_template('temp.html',ur=ur,ul=ul,lower = lower,a=a,b=b,c=c,d=d,e=e,f=f)

if __name__=='__main__':
    app.debug = True
    app.run()
