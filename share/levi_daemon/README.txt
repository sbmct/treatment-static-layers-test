
To run the levi's daemons & get the live data to display in store:

#SETUP THE NODE ENV
cd ~/src/sluice-data/treatment-nrf-2016/share/levi_daemon
npm install zmq
npm install ws
npm install request  <-- this one may not be needed...try without first.

#RUN THE NODE DAEMON
#Then you're ready to run the node script which subscribes to the levi's ws.
node levis_auth_zmq_daemon.js
  ** Note as of today authentication is still manual, so expect an error until you manually enter the auth info in the script. Fixing that presently.


#SETUP THE PYTHON ENVIRONMENT
cd ~/src/sluice-data/treatment-nrf-2016/share/levi_daemon
../../scripts/venvdo pip install zmq

#RUN THE PYTHON DAEMON
../../scripts/venvdo python sensor_processor.py


#RUN THE FLASK WEB SERVER
cd ~/src/sluice-data/treatment-nrf-2016/share/flask
python app.py
