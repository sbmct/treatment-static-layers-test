var request = require('request');
var WebSocket = require('ws');
var http = require('http');
var zmq = require('zmq');

// socket to talk to server
console.log("Connecting to the local Conduce zmq server…");
var requester = zmq.socket('req');

requester.on("message", function(reply) {
  console.log("Received reply from py zmq server: [", reply.toString(), ']');
});

requester.connect("tcp://localhost:5555");

process.on('SIGINT', function() {
  requester.close();
});


// This function handles authentication (and will do reauthentication on error), web socket subscription, & sending of the data via zmq to the python script
function doRequest() {

websocket = 'ws://levis-final.demotrustedanalytics.com/ws';
var options = {
    headers: {
Authorization: 'Digest username="intel", realm="Levis", nonce="1452111773:e2d58c6cf8772cf3821215ea3cbd2cfd", uri="/ws", algorithm=MD5, response="6c4c5b1b9d65b82e0cbb6261c24b9f2a", qop=auth, nc=00000048, cnonce="ea13383d9aecf0e8"'
    }
}

ws = new WebSocket(websocket, options);
console.log('created a new WebSocket');

ws.on('open', function open() {
  console.log('connected');
});
 
ws.on('close', function close() {
  console.log('disconnected');
});
 
ws.on('message', function message(data, flags) {
    data = data.toString();
    console.log(data);
  console.log("Sending request over zmq to python server");
  requester.send(data);
});

ws.on('error', function (err) {
    console.log("error");
    //console.log(typeof err);
    //console.log(err);
/*

    var options = {
host: 'levis-final.demotrustedanalytics.com',
path: '/'
};

    http.get(options, function(res) {
        console.log("Got response: " + res.statusCode);

        for(var item in res.headers) {
            console.log(item + ": " + res.headers[item]);
        }
        auth_data = res.headers['www-authenticate'];
        console.log("AUTH");
        console.log(auth_data);

            doRequest();
        
        }).on('error', function(e) {
            console.log("Got error: " + e.message);
        });

//THE header:
//www-authenticate: Digest realm="Levis", nonce="1451877071:ad921d0f536e354e08c1b67609462520", algorithm="MD5", qop="auth"
//x-cf-requestid: a2426f88-fcce-4651-5665-0f202f9b31ac
*/
});

}

// This call starts the process, and the ws.on(error) function will re-call doRequest() once it has sorted the re-authentication via http.
doRequest();
