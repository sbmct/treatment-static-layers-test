from sluice.types import v3float64
from sluice import Sluice
import csv
import time
from random import randint
import time
import zmq
import json    # or `import simplejson as json` if on Python < 2.6


sensors = {'SN01089':(37.801787189015457,-122.40301225466975,37.801909883792433,-122.40272099956236,37.801902519999999,-122.4030071),'SN01103':(37.801735790706303,-122.40301213525393,37.801858485568673,-122.40272088014653,37.801851229999997,-122.40300190000001),'SN01080':(37.801764242028277,-122.40298008144197,37.801886259503711,-122.40269043421668,37.801880079999997,-122.402973),'SN01075':(37.801787690183751,-122.40295866880284,37.801909707620453,-122.40266902157757,37.801902519999999,-122.4029464),'SN01082':(37.801787838595828,-122.40293707121762,37.801909856032275,-122.40264742399233,37.801902519999999,-122.40292909999999),'SN01106':(37.801760110273626,-122.40294779083402,37.801882127755889,-122.40265814360873,37.801874589999997,-122.4029441),'SN01077':(37.801737064534656,-122.40295837627384,37.801859082054989,-122.40266872904856,37.801851229999997,-122.4029508),'SN01073':(37.801795961984581,-122.40288366562289,37.801917979407627,-122.40259401839761,37.80191076,-122.4028721),'SN01048':(37.801795670864905,-122.40285175429108,37.80191768828842,-122.40256210706579,37.801910759999998,-122.4028418),'SN01070':(37.801762181383474,-122.40290457390532,37.801884198862311,-122.40261492668004,37.801878250000001,-122.40290160000001),'SN01076':(37.801751651734769,-122.40288336313536,37.801873669231,-122.40259371591007,37.801867260000002,-122.4028721),'SN01107':(37.801761704344656,-122.40285188048198,37.801884399163974,-122.40256062537459,37.801878250000001,-122.4028418),'SN01086':(37.801740952742534,-122.4028510725274,37.801862970256451,-122.40256142530211,37.801855359999998,-122.4028418),'SN01074':(37.801736534627153,-122.40290470566428,37.80185855214836,-122.402615058439,37.801850780000002,-122.40290160000001),'SN01087':(37.80172191507782,-122.40288337522335,37.801843932623171,-122.40259372799807,37.80183658,-122.4028721),'SN01090':(37.801704866702678,-122.40290475764257,37.801826884276196,-122.40261511041729,37.801819639999998,-122.40290160000001),'SN01083':(37.801702697601087,-122.40285124780299,37.801824715178192,-122.4025616005777,37.801817579999998,-122.4028418),'SN01104':(37.801692370583694,-122.40288297148496,37.801814388177853,-122.40259332425967,37.80180636,-122.4028721),'SN01101':(37.801651948216772,-122.40288334983856,37.801773965877715,-122.40259370261327,37.801766299999997,-122.4028721),'SN01068':(37.801655903024376,-122.40290470687302,37.801777920678795,-122.40261505964773,37.801770650000002,-122.40290160000001),'SN01085':(37.801656231243904,-122.40285136989145,37.801778248897776,-122.40256172266616,37.801770650000002,-122.4028418)}
def fake(sensor,mens,womens,accessories):
    record= {
	'id': sensor,
	'attrs': {'mens': mens,'womens':womens,'accessories':accessories}
    }
    return record

def generate(s):
    data =[]

    for i in sensors:
        mens = randint(0,9)
        womens = randint(0,9)
        accessories = randint(0,9)
        s.request_web_fluoro(i,"http://localhost:5000/"+str(mens)+"/"+str(womens)+"/"+str(accessories),bl=[sensors[i][0],sensors[i][1]],tr=[sensors[i][2],sensors[i][3]])
        data.append(fake(i,mens,womens,accessories))
    print data
    return data

#{"counts": {"SN01086": {"LEVIS MENS": 1}, "SN01104": {"LEVIS MENS": 1, "LEVIS WOMEN": 6}, "SN01074": {"LEVIS WOMEN": 1}, "SN01106": {"LEVIS WOMEN": 1, "LEVIS MENS": 4}, "SN01103": {"LEVIS MENS": 1}, "SN01090": {"LEVIS WOMEN": 2, "LEVIS MENS": 28}, "SN01085": {"LEVIS MENS": 1, "LEVIS WOMEN": 9}, "SN01087": {"LEVIS WOMEN": 1, "LEVIS MENS": 8}, "SN01068": {"LEVIS ACCESSORIES": 1, "LEVIS MENS": 19}, "SN01101": {"LEVIS MENS": 7, "Dockers Men": 4, "LEVIS WOMEN": 4}, "SN01083": {"LEVIS MENS": 1, "LEVIS WOMEN": 6}, "SN01082": {"LEVIS MENS": 3, "LEVIS ACCESSORIES": 1, "LEVIS WOMEN": 6}, "SN01070": {"LEVIS WOMEN": 1}, "SN01048": {"Dockers Men": 2, "LEVIS MENS": 5, "LEVIS WOMEN": 1}, "SN01089": {"LEVIS WOMEN": 4, "LEVIS ACCESSORIES": 1, "LEVIS MENS": 25}, "SN01075": {"Dockers Men": 1, "LEVIS WOMEN": 2, "LEVIS MENS": 4}, "SN01077": {"LEVIS MENS": 1, "Dockers Men": 2}}, "timestamp": 1442713710, "channel": "out_of_place_counts_by_location_and_div", "period": 10}

def process_sensor_data(message, s):
    #message = json.loads(message)    # obj now contains a dict of the data

    data = []

    if "counts" in message:
        for sensor in message["counts"]:
            item_dict = message["counts"][sensor]
            print sensor
            #print item_dict
            attrs = {'mens': 0,'womens': 0,'accessories': 0}
            if "LEVIS MENS" in item_dict:
                print "\tMENS: ", item_dict["LEVIS MENS"]
                attrs['mens'] = item_dict["LEVIS MENS"]
            if "LEVIS WOMEN" in item_dict:
                print "\tWOMENS: ", item_dict["LEVIS WOMEN"]
                attrs['womens'] = item_dict["LEVIS WOMEN"]
            if "LEVIS ACCESSORIES" in item_dict:
                print "\tAccessories: ", item_dict["LEVIS ACCESSORIES"]
                attrs['accessories'] = item_dict["LEVIS ACCESSORIES"]

            record = {
        	'id': sensor,
            'attrs': attrs
            }
            data.append(record)
            #print "DATA: ", data

            ####save mens, womens, accessories
            ####for each item, request an updated webpage, *bonus points if you only do it when the values have changed
            s.request_web_fluoro(sensor,"http://localhost:5000/"+str(attrs['mens'])+"/"+str(attrs['womens'])+"/"+str(attrs['accessories']),bl=[sensors[sensor][0],sensors[sensor][1]],tr=[sensors[sensor][2],sensors[sensor][3]])

        ####we have all the data, so update the roundels
        s.update_observations(data);

    #print message
    return message


######## a sample message to drive path ########
#{"last_read":1442734936336000,"upc":"052177964067","epc":"3034032F445E258BA43B7443","facility_id":"levi501","location":"SN01107","event":"moved","product":{"upc":"052177964067","product_code":"005011764","waist":"30W","length":"32L","short_name":"501 LEVIS ORIGINAL FIT WHITE LIGHT","product_name":"LEVI STRAUSS & COMPANY","div_desc":"LEVIS MENS","dept_desc":"MEN'S BOTTOMS","sub_dept_desc":"CORE","class_desc":"501"},"location_name":"Women's Jeans (L)","misplacement_score":2.106418761785035,"misplacement_reason":"distance to primary","valid_locations":[{"location":"SN01070","weight":0.9629629629629629,"location_name":"Display Men's Jeans (R)"},{"location":"SN01076","weight":0.037037037037037035,"location_name":"Women's Contour Jeans"}],"is_out_of_place":true,"connected_component_key":"product_code","channel":"out_of_place_changed","userignore":true}

def process_path_data(message):
    path = []
    sensor = message['location']
    lat1 = sensors[sensor][4]
    lon1 = sensors[sensor][5]
    sensor2 = message['valid_locations'][0]['location']
    lat2 = sensors[sensor2][4]  
    lon2 = sensors[sensor2][5]
    loc1 = v3float64(float(lat1),float(lon1),0.0)
    loc2 = v3float64(float(lat2),float(lon2),0.0)
    path.append(loc1)
    path.append(loc2)
    print path
    topo = {
    	'id': 'move-path',
        'kind': 'move-path',
    	'path': path,
    	'attrs': {
    	}
        }  
    print topo
    return topo


def main():

    context = zmq.Context()
    socket = context.socket(zmq.REP)
    socket.bind("tcp://*:5555")


    s = Sluice()
    while True:
        #data = generate(s)
        #s.update_observations(data)

        message = socket.recv()
        #  Send reply back to client
        socket.send("ack")
        print("Received request")
        #print("Received request: %s" % message)
        if message:
#            print 'message here'
#            print str(message)
#            print 'eom'

            message = json.loads(message)    # obj now contains a dict of the data


            if 'channel' in message:
                print str(message['channel'])

                if message['channel'] == 'out_of_place_counts_by_location_and_div':
                    data = process_sensor_data(message, s)
                if message['channel'] == 'out_of_place_changed':
                    if 'userignore' in message:
                        #userignore only appears if the message was clicked int the Intel app, not if it was in the event stream
                        print '\t#####################################################User clicked to return an item!'
                        print '\t', str(message)
                        s.update_topology([process_path_data(message)])
                        
                    else:
                        print '\tOut of place changed, not user input'
                else:
                    print "\tOther channel: ", message['channel']

        time.sleep(0.01)

if __name__=="__main__":
    main()
