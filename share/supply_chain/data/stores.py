import csv, random
import json
from sluice.types import v3float64
import os.path
from supply_chain import time_ref
from datetime import datetime, timedelta
import dist_centers as dc

def bounded(mean, stddev, minval, maxval):
    out = max(minval, min(maxval, random.gauss(mean, stddev)))
    return out

IGNORE_ATTRS = ('store type', 'store ident', 'misc info')

#[stock, sales, customers, employees]
#0 = green
#1 = yellow-ish
#2 = red-ish
#3 = red (not redish)
#Dec 1: good
#Dec 5: stock impacted, sales to follow
#Dec 10: stock and sales bad
#Dec 15: Reouting - stores unaffected
#Dec 17: Distro Centers better - stores unaffected
#Dec 18: Stock yellow (improving)
#Dec 19: Stock green
#Dec 21: Sales improving
EVENTS = [
    ('2014-12-01', [0, 0, 0, 0]),
    ('2014-12-03', [1, 0, 0, 0]),
    ('2014-12-05', [2, 1, 0, 0]),
    ('2014-12-07', [3, 2, 1, 0]),
    ('2014-12-10', [3, 2, 2, 0]),
    ('2014-12-15', [3, 3, 2, 0]),
    ('2014-12-17', [2, 2, 2, 0]),
    ('2014-12-18', [1, 2, 2, 0]),
    ('2014-12-19', [1, 1, 1, 0]),
    ('2014-12-21', [0, 1, 1, 0]),
    ('2014-12-23', [0, 0, 0, 0]),
    ('2014-12-31', [0, 0, 0, 0]),
]
#Friendler with code
EVENTS = [(datetime.strptime(ev[0], '%Y-%m-%d'), ev[1]) for ev in EVENTS]
EPOCH_TIME = datetime(1970,1,1)
def epoch_time(ts):
    return (ts - EPOCH_TIME).total_seconds()

class Store(object):
    def __init__(self, data):
        self.kind = data['store type']
        self.state = data.get('state')
        self.ident = data['store ident']
        misc_attrs = dict((unicode(k), unicode(v[:64])) for (k, v) in json.loads(data['misc info']).iteritems())
        self.attrs = misc_attrs
        (self.attrs.update(data[d]) for d in data if d not in IGNORE_ATTRS)
        self.attrs['address'] = unicode('%s\n%s, %s %s' % (data.get('address'),
                                                           data.get('city'),
                                                           data.get('state'),
                                                           data.get('zip code')))
        self.latitude = float(data['latitude'])
        self.longitude = float(data['longitude'])
        self.loc = v3float64(self.latitude,
                             self.longitude, 0.0)
        self.timestamp = 0.0
        self.available_distros = []

        #Make state switching a little slower
        self.metric_state = [0, 0, 0, 0]
        self.means = [0, 0, 0, 0]
        self.stds = [0, 0, 0, 0]
        self._set_means()
        self.pending_state = None
        self.last_update = None

    def set_next_state(self, next_state, force=False):
        self.pending_state = next_state
        return self.update_state_coin_flip(force)

    def _set_means(self):
        if self.metric_state[0] == 0:
            #5% is the green-yellow edge value, set to that ~20% are in the yellow/red
            self.means[0] = 0
            self.stds[0] = 2
        elif self.metric_state[0] == 1:
            #put into the yellow (2-3.5)
            self.means[0] = bounded(2.75, 0.3, 1.8, 3.6)
            self.stds[0] = 0.01
        elif self.metric_state[0] == 2:
            #yellow-red edge is 3.5
            self.means[0] = bounded(4.0, 0.4, 3.45, 5)
            self.stds[0] = 0.01
        else:
            self.means[0] = bounded(5.0, 0.3, 4.0, 6.0)
            self.stds[0] = 0.01

        if self.metric_state[1] == 0:
            #green is 66-100
            self.means[1] = 75
            self.stds[1] = 15
        elif self.metric_state[1] == 1:
            #yellow is 33-66, put in the middle
            self.means[1] = bounded(50, 7, 40, 60)
            self.stds[1] = 0
        elif self.metric_state[1] == 2:
            #yellow is 33-66, put just in the red
            self.means[1] = bounded(32, 3.3, 30, 35)
            self.stds[1] = 0.01
        else:
            self.means[1] = bounded(25, 3.3, 20, 30)
            self.stds[1] = 0.01

        if self.metric_state[2] == 0:
            self.means[2] = 75
            self.stds[2] = 10
        elif self.metric_state[2] == 1:
            self.means[1] = bounded(50, 7, 40, 60)
            self.stds[2] = 0
        elif self.metric_state[2] == 2:
            #yellow is 33-66
            self.means[2] = bounded(32, 3.3, 20, 35)
            self.stds[2] = 0.01
        else:
            self.means[2] = bounded(25, 3.3, 20, 30)
            self.stds[2] = 0.01

        #just keep this the same always
        self.means[3] = 33-(1.25*15.0)
        self.stds[3] = 15 #just keep this the same always


    def update_state_coin_flip(self, force=False):
        if self.pending_state is not None:
            if force or random.random() < 0.3:
                self.metric_state = self.pending_state
                self.pending_state = None
                self.last_update = None
                self._set_means()
                return True
        return False

    def set_store_stats(self, ts, cache_time=None):
        #Flip the coin if there is a pending state - may reset last_update
        self.update_state_coin_flip()

        if isinstance(ts, (float, int)):
            fts = ts
            ts = datetime.utcfromtimestamp(ts)
        else:
            fts = epoch_time(ts)

        #Make the states consistent across different machines
        if cache_time is not None:
            if self.last_update is not None and self.last_update > cache_time:
                #The last update is more recent than the cache time
                return False

        self.timestamp = fts
        self.last_update = ts

        #There is a little lie, we just always make employees good (maybe change this for a weather demo)
        self.attrs['Average OOS %'] = str(bounded(self.means[0], self.means[0], 0, 20))
        self.attrs['Sales / sq. ft.'] = str(bounded(self.means[1], self.stds[1], 5, 100))
        #67 is the green-yellow edge value, so set the mean so that ~2-3% of stores are yellow/red
        self.attrs['Shoppers / 1000 sq. ft.'] = str(bounded(self.means[2], self.stds[2], 25, 100))
        #33 is the green-yellow edge value (smaller = green), so set the mean so that ~2-3% of nodes are yellow/red
        self.attrs['Staff / 1000 sq. ft.'] = str(bounded(self.means[3], self.stds[3], 10, 100))
        return True

    def to_topo_slaw(self):
        return {
            'timestamp' : self.timestamp,
            'id'        : self.ident,
            'kind'      : self.kind,
            'loc'       : self.loc,
            'attrs'     : self.attrs
        }

    def to_obs_slaw(self):
        attrs = {}
        attrs['Sales / sq. ft.']          = self.attrs['Sales / sq. ft.']
        attrs['Average OOS %']            = self.attrs['Average OOS %']
        attrs['Shoppers / 1000 sq. ft.']  = self.attrs['Shoppers / 1000 sq. ft.']
        attrs['Staff / 1000 sq. ft.']     = self.attrs['Staff / 1000 sq. ft.']
        if 'event' in self.attrs:
            attrs['event'] = self.attrs['event']

        return {
            'timestamp' : self.timestamp,
            'id'        : self.ident,
            'attrs'     : attrs
        }

    def __repr__(self):
        return str(self.to_topo_slaw())


def make_obs(stores):
    #assume green before and after
    for store in stores:
        if len(store.available_distros) == 0:
            continue
        if 'stock' in store.available_distros[0]['distro']:
            event = 0
            last_event = 0
            distro = store.available_distros[0]['distro']
            oos_days = 0
            recover_days = 0
            for x in distro['stock']:
                if last_event == 10:
                    break
                if oos_days > 0 and x['stock'] > 0.2:
                    recover_days += 1
                if recover_days > 6:
                    event = 10
                elif recover_days > 4:
                    event = 9
                elif recover_days > 3:
                    event = 8
                elif recover_days > 2:
                    event = 7
                elif recover_days > 0:
                    event = 6
                elif x['stock'] >= 0.5:
                    oos_days = 0
                elif x['stock'] >= 0.2:
                    event = 1
                elif x['stock'] > 0:
                    event = 2
                else:
                    oos_days += 1
                    if oos_days > 3:
                        event = 5
                    elif oos_days > 1:
                        event = 4
                    else:
                        event = 3
                if event <= last_event:
                    continue
                for t in xrange(4):
                    if store.set_next_state(EVENTS[event][1]):
                        store.set_store_stats(x['date']+86400+(21600*t))
                        store.attrs['event'] = str(event)
                        last_event = event
                        yield store.to_obs_slaw()
                        break
        else:
            cur_time = EVENTS[0][0]
            while cur_time < EVENTS[-2][0]+timedelta(days=2):
                cur_time += timedelta(hours=6)
                #25% chance the store changes at all over a ~20 day period
                #20 days * 4 attmpets per day = ~80 rolls
                if (random.random() < 0.003):
                    store.set_store_stats(cur_time)
                    yield store.to_obs_slaw()

def all_stores(stores):
    for d in stores:
        try:
            yield Store(d)
        except UnicodeDecodeError:
            pass

