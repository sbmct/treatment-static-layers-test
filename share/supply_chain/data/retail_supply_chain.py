from sluice import Sluice
from supply_chain import time_ref
import conduce_csv as ccsv
import stores as st
import worldports as wp
import shipping_routes as sr
import dist_centers as dc
import suppliers as sp
import os.path
import datetime, calendar
import random

TOPO_PATH = os.path.join(os.path.dirname(__file__), 'topo')
SOURCE_DIR = os.path.join(os.path.dirname(__file__), 'source')

SUPPLIERS = os.path.join(SOURCE_DIR, 'overseas_suppliers.csv')
PORTS = os.path.join(SOURCE_DIR, 'worldports.csv')
DISTROS = os.path.join(SOURCE_DIR, 'clean_distros.csv')
STORES = os.path.join(SOURCE_DIR, 'all_stores.csv')
SEAROUTES = os.path.join(SOURCE_DIR, 'searoutes.csv')

def output(x):
    output = open(os.path.join(TOPO_PATH,x+'.yaml'),'w')
    return output

def make_ports(ports):
    print 'make_ports'
    s = Sluice(output=output('worldports'))
    s.inject_topology(ccsv.dict_to_protein(ports,wp.translate_record))
    s.update_observations(ccsv.dict_to_protein(ports, wp.simulate_obs))
    s.inject_topology(list(wp.harbor_topo()))
    s.update_observations(list(wp.harbor_obs()))
    print 'ports injected'

def make_suppliers(suppliers):
    print 'make_suppliers'
    s = Sluice(output=output('suppliers'))
    s.inject_topology(ccsv.dict_to_protein(suppliers, sp.translate_record))
    print 'suppliers injected'

def make_stores(stores):
    print 'make_stores'
    s = Sluice(output=output('stores'))
    s.inject_topology(store.to_topo_slaw() for store in stores)
    make_storeobs(stores)
    print 'stores injected'

def make_storeobs(stores):
    print 'make_storeobs'
    s = Sluice(output=output('stores_obs'))
    s.update_observations(st.make_obs(stores))
    print 'store obs injected'

def make_distros(integrators):
    print 'make_distros'
    s = Sluice(output=output('dist_centers'))
    s.inject_topology(ccsv.dict_to_protein(integrators, dc.translate_record))
    s.update_observations(ccsv.dict_to_protein(integrators, dc.simulate_obs))
    print 'distribution centers injected'

def make_shippingroutes(ports, suppliers, integrators, stores, seaRoutes):
    print 'make_shippingroutes'
    sr.gen_port_routes(ports, seaRoutes)
    sr.find_accessible_ports(ports, suppliers, integrators)
    sr.find_accessible_distros(integrators, stores)

    s = Sluice(output=output('shipping_routes'))
    s.inject_topology(sr.generate_routes(stores, suppliers, integrators, seaRoutes))
    s.update_observations(sr.degrade_routes(integrators))
    print 'shipping routes injected'

def make_delays(ports):
    print 'make_delays'
    ports_by_name = dict((p['name'], p) for p in ports if p['country'] == 'US')
    f = open(os.path.join(SOURCE_DIR, 'delay_sim.csv'))
    keys = f.next().strip().split(',')
    for line in f:
        row = dict(zip(keys, line.strip().split(',')))
        row['date'] = calendar.timegm(datetime.datetime.strptime(row['date'], '%m/%d/%y').utctimetuple())
        for p in keys[1:]:
            port = ports_by_name[p]
            if 'delay' not in port:
                port['delay'] = []
            port['delay'].append({ 'date': row['date'], 'days': int(row[p]) })

if __name__ == '__main__':
    with open(PORTS, 'r') as f:
        ports = ccsv.parse_csv(f)
    with open(SUPPLIERS, 'r') as f:
        suppliers = ccsv.parse_csv(f)
    with open(DISTROS, 'r') as f:
        integrators = ccsv.parse_csv(f)
    with open(STORES, 'r') as f:
        stores = ccsv.parse_csv(f)
    with open(SEAROUTES, 'r') as f:
        sea = ccsv.parse_csv(f)

    random.seed(999) #same output over multiple runs/machines
    make_delays(ports)
    stores = list(st.all_stores(stores))
    for store in stores:
         #Start out with green stores all around
         store.set_store_stats(time_ref.time_start())

    make_shippingroutes(ports, suppliers, integrators, stores, sea)
    make_distros(integrators)
    make_stores(stores)
    make_ports(ports)
    make_suppliers(suppliers)
