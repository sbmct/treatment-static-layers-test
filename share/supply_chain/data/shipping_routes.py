import itertools
import os.path
from sluice.types import float64
import math
import datetime, calendar

LAND_SPEED_KMPH = 55 * 1.60934
SEA_SPEED_KMPH = 21 * 1.825
END_OF_THE_WORLD_LON = 65
VOWELS = ['a','e','i','o','u']
STORE_ROUTE_KIND = "store-route"
SUPPLIER_ROUTE_KIND = "supplier-route"
INTEGRATOR_ROUTE_KIND = "integrator-route"
CARGO_ROUTE_KIND = "shipping-route"
REROUTE_DATE = {
    'NORFOLK': calendar.timegm(datetime.datetime(2014, 12, 15).utctimetuple()),
    'HOUSTON': calendar.timegm(datetime.datetime(2014, 12, 16).utctimetuple()),
    'NEW ORLEANS': calendar.timegm(datetime.datetime(2014, 12, 17).utctimetuple()),
}

def route_id(landmark, port):
    return landmark['name']+':'+port['name']

def degrade_routes(distros):
    for distro in distros:
        port = distro['available_ports'][0]['port']
        routeId = route_id(distro, port)
        rerouteId = route_id(distro, distro['available_ports'][-1]['port'])
        rname = distro['available_ports'][-1]['port']['name']
        if 'delay' in port:
            status = ['Normal']
            deliveries = []
            orig_days = round(distro['available_ports'][0]['duration_h'] / 24.0)
            new_days = round(distro['available_ports'][-1]['duration_h'] / 24.0)
            distro['delay'] = []
            suspended = False
            stopped = False
            slowed = False
            size = 1.0
            for d in port['delay']:
                shipping_days = d['days'] + orig_days
                if d['date'] < REROUTE_DATE[rname]:
                    if d['days'] <= 1:
                        if status[-1] != 'Normal':
                            yield {
                                'id': routeId,
                                'timestamp': d['date'],
                                'attrs': { 'status': 'Normal' }
                            }
                            status.append('Normal')
                    elif shipping_days < 5:
                        if status[-1] != 'Slow':
                            yield {
                                'id': routeId,
                                'timestamp': d['date'],
                                'attrs': { 'status': 'Slow' }
                            }
                            status.append('Slow')
                            size = 0.2
                    else:
                        if status[-1] != 'Stopped':
                            yield {
                                'id': routeId,
                                'timestamp': d['date'],
                                'attrs': { 'status': 'Stopped' }
                            }
                            status.append('Stopped')
                    if status[-1] != 'Stopped':
                        deliveries.append((d['date'] + 86400 * shipping_days, size))
                elif 'Suspended' not in status:
                    if new_days < shipping_days or d['days'] >= 5:
                        yield {
                            'id': routeId,
                            'timestamp': d['date'],
                            'attrs': { 'status': 'Suspended' },
                        }
                        yield {
                            'id': rerouteId,
                            'timestamp': d['date'] + distro['available_ports'][-1]['duration_h'] * 3600,
                            'attrs': { 'status': 'Normal' }
                        }
                        status.append('Suspended')
                        size = 0.3
                        deliveries.append((d['date'] + 86400 * new_days, size))
                    elif d['days'] <= 1:
                        if status[-1] != 'Normal':
                            yield {
                                'id': routeId,
                                'timestamp': d['date'],
                                'attrs': { 'status': 'Normal' }
                            }
                        deliveries.append((d['date'] + 86400 * shipping_days, size))
                        size = 1.0
                    elif d['days'] < 5:
                        if status[-1] != 'Slow':
                            yield {
                                'id': routeId,
                                'timestamp': d['date'],
                                'attrs': { 'status': 'Slow' }
                            }
                            size = 0.2
                        deliveries.append((d['date'] + 86400 * shipping_days, size))
                elif status[-1] == 'Suspended':
                    if d['days'] <= 1:
                        yield {
                            'id': routeId,
                            'timestamp': d['date'] + (d['days'] * 86400) + (distro['available_ports'][0]['duration_h'] * 3600),
                            'attrs': { 'status': 'Normal' }
                        }
                        yield {
                            'id': rerouteId,
                            'timestamp': d['date'] + (d['days'] * 86400) + (distro['available_ports'][0]['duration_h'] * 3600),
                            'attrs': { 'status': 'Removed' }
                        }
                        size = 1.0
                        status.append('Normal')
                        deliveries.append((d['date'] + 86400 * shipping_days, size))
                    else:
                        deliveries.append((d['date'] + 86400 * new_days, size))
                else:
                    if d['days'] <= 1:
                        if status[-1] != 'Normal':
                            yield {
                                'id': routeId,
                                'timestamp': d['date'],
                                'attrs': { 'status': 'Normal' }
                            }
                            status.append('Normal')
                            size = 1.0
                    elif shipping_days < 5:
                        if status[-1] != 'Slow':
                            yield {
                                'id': routeId,
                                'timestamp': d['date'],
                                'attrs': { 'status': 'Slow' }
                            }
                            status.append('Slow')
                            size = 0.2
                    else:
                        if status[-1] != 'Stopped':
                            yield {
                                'id': routeId,
                                'timestamp': d['date'],
                                'attrs': { 'status': 'Stopped' }
                            }
                            status.append('Stopped')
                    if status[-1] != 'Stopped':
                        deliveries.append((d['date'] + 86400 * shipping_days, size))
            deliveries = sorted(deliveries)
            stock = 1.0
            distro['stock'] = []
            date = port['delay'][0]['date']
            while len(deliveries) > 0:
                stock -= 0.2
                size = 0
                while len(deliveries) > 0 and deliveries[0][0] <= date:
                    dlv = deliveries.pop(0)
                    size = dlv[1]
                if size == 1.0:
                    stock = 1.0
                else:
                    stock += size
                stock = max(0, min(1.2, stock))
                distro['stock'].append({
                    'date': date,
                    'stock': stock,
                    'delivery': size
                })
                date += 86400

def generate_land_routes(landmarks, kind):
    for landmark in landmarks:
        for i, port in enumerate(landmark['available_ports']):
            routeId = landmark['name'] + ":" + port['port']['name']
            landmarkLoc = [float64(landmark['latitude']), float64(landmark['longitude']), 0]
            portLoc = [float64(port['port']['latitude']), float64(port['port']['longitude']), 0]
            if landmarkLoc[1] > END_OF_THE_WORLD_LON:
                landmarkLoc[1] -= 360
            if portLoc[1] > END_OF_THE_WORLD_LON:
                portLoc[1] -= 360
            yield {
                    "id": routeId,
                    "kind": kind,
                    "timestamp": port.get('time', 0),
                    "path": [landmarkLoc, portLoc],
                    "attrs": {
                        "rank": port.get('rank', str(i)),
                        "status": port.get('status', "Normal")
                    }
            }
    return

def generate_distro_to_store(landmarks, kind):
    ranks = [0,1,1,1,1,1,1]
    for landmark in landmarks:
        for i, distro in enumerate(landmark.available_distros[:3]):
            routeId = landmark.ident + ":" + distro['distro']['name']
            landmarkLoc = [float64(landmark.latitude), float64(landmark.longitude), 0]
            distroLoc = [float64(distro['distro']['latitude']), float64(distro['distro']['longitude']), 0]
            if landmarkLoc[1] > END_OF_THE_WORLD_LON:
                landmarkLoc[1] -= 360
            if distroLoc[1] > END_OF_THE_WORLD_LON:
                distroLoc[1] -= 360
            yield {
                    "id": routeId,
                    "kind": kind,
                    "timestamp": 0,
                    "path": [landmarkLoc, distroLoc],
                    "attrs": {
                        "rank": str(ranks[i])
                    }
            }
    return

def generate_sea_routes(seaRoutes):
    for route in seaRoutes:
        routeId = route['origin']['name'] + ":" + route['destination']['name']
        path = list([x[0], x[1], 0.0] for x in route['waypoints'])
        for x in path:
            if x[1] > END_OF_THE_WORLD_LON:
                x[1] -= 360
        yield {
                "id": routeId,
                "kind": CARGO_ROUTE_KIND,
                "timestamp": 0,
                "path": path,
                "attrs": {
                    "rank": str(route['rank']),
                }
        }
    return


def generate_routes(stores, suppliers, integrators, seaRoutes):
    return itertools.chain(generate_land_routes(suppliers, SUPPLIER_ROUTE_KIND),
            generate_land_routes(integrators, INTEGRATOR_ROUTE_KIND),
            generate_distro_to_store(stores, STORE_ROUTE_KIND),
            generate_sea_routes(seaRoutes))


def distance_equirectangular(coordA, coordB):
    coordA_rad = (math.radians(coordA[0]), math.radians(coordA[1]))
    coordB_rad = (math.radians(coordB[0]), math.radians(coordB[1]))
    latDelta = (coordA_rad[0] - coordB_rad[0])
    lonDelta = (coordA_rad[1] - coordB_rad[1])
    latMean = (coordA_rad[0] + coordB_rad[0]) / 2
    xDelta = lonDelta * math.cos(latMean)
    yDelta = latDelta
    R_km = 6371
    return R_km * math.sqrt(xDelta**2 + yDelta**2)


def get_distance(origin, destination):
    return distance_equirectangular((float64(origin['latitude']), float64(origin['longitude'])),
            (float64(destination['latitude']), float64(destination['longitude'])))

def alt_get_distance(origin, lat, lon):
    return distance_equirectangular((float64(origin['latitude']), float64(origin['longitude'])),
            (float64(lat), float64(lon)))


def find_accessible_ports_refactor(ports, locations):
    for location in locations:
        location['available_ports'] = []

    for location in locations:
        for port in ports:
            if location['country'] == port['country']:
                distance_km = get_distance(port, location)
                duration_h = distance_km / LAND_SPEED_KMPH
                location['available_ports'].append(dict(port=port, distance_km=distance_km, duration_h=duration_h))
    return


def find_accessible_ports(ports, suppliers, integrators):
    usports = dict((port['name'], port) for port in ports if port['country'] == 'US')
    intports = dict(('%s, %s' % (port['name'], port['country']), port) for port in ports if port['country'] != 'US')
    for dist in integrators:
        dist['available_ports'] = list()
        for k in ('p1', 'p2', 'p3', 'p4', 'backup'):
            port = usports.get(dist.get(k))
            if not port:
                continue
            port['status'] = 'Major'
            distance_km = get_distance(port, dist)
            duration_h = distance_km / LAND_SPEED_KMPH
            p = {
                'port': port,
                'distance_km': distance_km,
                'duration_h': duration_h
            }
            if k == 'backup':
                p['rank'] = 0
                p['status'] = 'Inactive'
            elif dist.get(k) == dist.get('backup'):
                continue
            dist['available_ports'].append(p)
    for sup in suppliers:
        sup['available_ports'] = list()
        for k in ('p1', 'p2'):
            port = intports.get(sup.get(k))
            if not port:
                continue
            port['status'] = 'Major'
            distance_km = get_distance(port, sup)
            duration_h = distance_km / LAND_SPEED_KMPH
            sup['available_ports'].append({
                'port': port,
                'distance_km': distance_km,
                'duration_h': duration_h
            })
    return

def find_accessible_distros(distros, locations):
    # ports = integrators
    # locations = stores

    for location in locations:
        if location.state in ('AK', 'HI', 'BC', 'AB', 'SK', 'MB', 'ON', 'QC'):
            continue
        for distro in distros:
            if location.state == distro['state']:
                distance_km = alt_get_distance(distro, location.latitude, location.longitude)
                duration_h = distance_km / LAND_SPEED_KMPH
                location.available_distros.append(dict(distro=distro, distance_km=distance_km, duration_h=duration_h))
        if len(location.available_distros) == 0:
            possible = []
            for distro in distros:
                distance_km = alt_get_distance(distro, location.latitude, location.longitude)
                duration_h = distance_km / LAND_SPEED_KMPH
                possible.append(dict(distro=distro, distance_km=distance_km, duration_h=duration_h))
            location.available_distros = sorted(possible, key=lambda x: x['distance_km'])[:2]
        location.available_distros = sorted(location.available_distros, key=lambda x: x['distance_km'])
    return

def gen_port_routes(ports, routes):
    usports = dict((p['name'], p) for p in ports if p['country'] == 'US')
    intports = dict(('%s, %s' % (p['name'], p['country']), p) for p in ports)
    for route in routes:
        origin = intports.get(route['origin'])
        if not origin:
            continue
        destination = usports.get(route['destination'])
        if not destination:
            continue
        origin['status'] = 'Major'
        destination['status'] = 'Major'
        route['origin'] = origin
        route['destination'] = destination
        route['waypoints'] = []
        route['waypoints'].append((float(origin['latitude']), float(origin['longitude'])))
        for k in ('wp1', 'wp2', 'wp3', 'wp4', 'wp5'):
            wp = route.get(k)
            if not wp:
                continue
            route['waypoints'].append(tuple(float(x.strip()) for x in wp.split(',')))
        route['waypoints'].append((float(destination['latitude']), float(destination['longitude'])))
        distance_km = 0.0
        for i in xrange(1, len(route['waypoints'])):
            distance_km += distance_equirectangular(route['waypoints'][i-1], route['waypoints'][i])
        duration_h = distance_km / SEA_SPEED_KMPH
        route['distance_km'] = distance_km
        route['duration_h'] = duration_h
    return

def find_interesting_ports(ports, harborSize, cornerA, cornerB):
    interestingPorts = []
    northwest = None
    southeast = None
    if cornerA[0] < cornerB[0] and cornerA[1] < cornerB[1]:
        southwest = cornerA
        northeast = cornerB
    elif cornerA[0] < cornerB[0] and cornerA[1] > cornerB[1]:
        southeast = cornerA
        northwest = cornerB
    elif cornerA[0] > cornerB[0] and cornerA[1] < cornerB[1]:
        northwest = cornerA
        southeast = cornerB
    elif cornerA[0] > cornerB[0] and cornerA[1] > cornerB[1]:
        northeast = cornerA
        southwest = cornerB

    if northwest == None:
        northwest = [northeast[0], southwest[1]]
        southeast = [southwest[0], northeast[1]]

    for port in ports:
        if port['harborsize'] == harborSize or harborSize == '*':
            if southeast[0] < float(port['latitude']) < northwest[0]:
                if northwest[1] < float(port['longitude']) < southeast[1]:
                    interestingPorts.append(port)

    return interestingPorts

def interesting_ports(ports):
    interestingUsWestPorts = find_interesting_ports(ports, 'L', [30,-110], [50,-140])
    interestingUsEastPorts = find_interesting_ports(ports, 'L', [30,-84], [50,-65])
    #interestingEuropePorts = find_interesting_ports(ports, 'L', [35,-14], [71,41])
    interestingEuropePorts = find_interesting_ports(ports, 'L', [35,-14], [59,26])
    interestingChinaPorts = find_interesting_ports(ports, 'L', [19,124], [39,105])
    interestingPorts = interestingUsWestPorts + interestingUsEastPorts + interestingEuropePorts + interestingChinaPorts
    return {'uswest':interestingUsWestPorts,
            'useast':interestingUsEastPorts,
            'europe':interestingEuropePorts,
            'china':interestingChinaPorts,
            'all':interestingPorts}
