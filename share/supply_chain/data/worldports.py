import csv
import os.path
import copy
from loam import v3float64, float64
import random
import datetime, calendar

CONGESTED = set([
    'LOS ANGELES',
    'LONG BEACH',
    'OAKLAND',
    'PORTLAND',
    'SEATTLE',
    'TACOMA',
])

HARBORS = {
    'LA': {
        'lat': 33.670747,
        'lon': -118.163429,
        'ships': {}
    },
    'SF': {
        'lat': 37.859680,
        'lon': -122.376342,
        'ships': {}
    },
    'SEATAC': {
        'lat': 47.716119,
        'lon': -122.456069,
        'ships': {}
    }
}
PORT_HARBORS = {
    'LOS ANGELES': 'LA',
    'LONG BEACH': 'LA',
    'OAKLAND': 'SF',
    'SEATTLE': 'SEATAC',
    'TACOMA': 'SEATAC'
}

def translate_record(record):
  # X,Y,INDEX_NO,REGION_NO,PORT_NAME,COUNTRY,LATITUDE,LONGITUDE,LAT_DEG,LAT_MIN,
  # LAT_HEMI,LONG_DEG,LONG_MIN,LONG_HEMI,PUB,CHART,HARBORSIZE,HARBORTYPE,SHELTER,
  # ENTRY_TIDE,ENTRYSWELL,ENTRY_ICE,ENTRYOTHER,OVERHD_LIM,CHAN_DEPTH,ANCH_DEPTH,
  # CARGODEPTH,OIL_DEPTH,TIDE_RANGE,MAX_VESSEL,HOLDGROUND,TURN_BASIN,PORTOFENTR,
  # US_REP,ETAMESSAGE,PILOT_REQD,PILOTAVAIL,LOC_ASSIST,PILOTADVSD,TUGSALVAGE,TUG_ASSIST,
  # PRATIQUE,SSCC_CERT,QUAR_OTHER,COMM_PHONE,COMM_FAX,COMM_RADIO,COMM_VHF,COMM_AIR,
  # COMM_RAIL,CARGOWHARF,CARGO_ANCH,CARGMDMOOR,CARBCHMOOR,CARICEMOOR,MED_FACIL,GARBAGE,
  # DEGAUSS,DRTYBALLST,CRANEFIXED,CRANEMOBIL,CRANEFLOAT,LIFT_100_,LIFT50_100,LIFT_25_49,
  # LIFT_0_24,LONGSHORE,ELECTRICAL,SERV_STEAM,NAV_EQUIP,ELECREPAIR,PROVISIONS,WATER,
  # FUEL_OIL,DIESEL,DECKSUPPLY,ENG_SUPPLY,REPAIRCODE,DRYDOCK,RAILWAY
  ts = 0
  ident = record['name']
  ent = { 'timestamp' : float64(ts),
          'kind' : 'port',
          'loc' : (v3float64(record['latitude'], record['longitude'], 0.0)),
          'id' : ident,
          'attrs' : {
             'Country': record['country'],
             'Port Name': ident,
             'Harbor Type': record['harbortype'],
             'Harbor Size': record['harborsize'],
             'Status': record.get('status', 'Minor'),
             'delay': '0',
             'ships': '0',
             'congested': 'false'
          }
        }
  #if record['name'] in CONGESTED and float(record['longitude']) < -100.0:
  #  ent['attrs']['congested'] = 'true'
  #else:
  #  ent['attrs']['congested'] = 'false'

  if float(record['longitude']) > -30:
    #put in a 2nd copy
    ent2 = copy.deepcopy(ent)
    ent2['id'] = '%s-west' % (ent['id'])
    ent2['loc'] = [float64(record['latitude']), float64(float(record['longitude'])-360.0), 0]
    yield ent2
  yield ent

def simulate_obs(record):
    if 'delay' in record:
        last_delay = 0
        last_count = 0
        for d in record['delay']:
            delta_delay = d['days'] - last_delay
            delta_ships = delta_delay * max(1.0, min(3.0, random.gauss(2.0, 1.0)))
            ships = last_count + delta_delay
            last_delay = d['days']
            last_count = ships
            harbor = HARBORS.get(PORT_HARBORS.get(record['name']))
            if harbor:
                if d['date'] not in harbor['ships']:
                    harbor['ships'][d['date']] = 0
                harbor['ships'][d['date']] += ships
            yield {
                'id': record['name'],
                'timestamp': d['date'],
                'attrs': {
                    'delay': str(d['days']),
                    'ships': str(ships),
                    'congested': 'true' if d['days'] > 1 else 'false'
                }
            }

def harbor_topo():
    for k, v in HARBORS.iteritems():
        size = len(list(1 for p, h in PORT_HARBORS.iteritems() if h == k))
        v['size'] = size
        yield {
            'timestamp': 0,
            'kind': 'harbor',
            'loc': v3float64(v['lat'], v['lon'], 0),
            'id': 'harbor-%s' % k,
            'attrs': { 'ships': '0', 'size': str(size), 'backlog': 'None' }
        }

def harbor_obs():
    for k, v in HARBORS.iteritems():
        for d, s in sorted(v['ships'].items()):
            if s / float(v['size']) >= 6.5:
                back = 'Extreme'
            elif s / float(v['size']) >= 4:
                back = 'Major'
            elif s / float(v['size']) >= 2:
                back = 'Minor'
            else:
                back = 'None'
            yield {
                'timestamp': d,
                'id': 'harbor-%s' % k,
                'attrs': { 'ships': str(s), 'backlog': back }
            }

