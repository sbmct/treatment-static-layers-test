import requests
#pip install beautifulsoup4
from bs4 import BeautifulSoup

def get_tables(url):
    response = requests.get(url)
    html = response.text
    soup = BeautifulSoup(html)

    #print len(soup.select('table'))
    all_data = []
    for table in soup.select('table'):
        if 'wikitable' not in table['class']:
            continue
        #print "parsing", table.findPreviousSibling()
        trs = iter(table.findAll('tr'))

        data = []
        data.append([th.text for th in trs.next().findAll('th')])
        for tr in trs:
            data.append([td.text for td in tr.findAll('td')])
        all_data.append(data)
    return all_data


#import csv
import unicodecsv as csv

def main():
    url = 'http://en.wikipedia.org/wiki/List_of_Ford_factories'
    all_data = get_tables(url)
    #I only care about the first table
    data = all_data[0]
    with open('plant_data.csv', 'wb') as csvfile:
        writer = csv.writer(csvfile)
        for row in data:
            writer.writerow(row)

if __name__ == '__main__':
    main()

