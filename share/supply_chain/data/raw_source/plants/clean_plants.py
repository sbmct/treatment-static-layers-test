import csv
from sluice.geocode import geo_lookup
import unicodedata
import os.path
import random

codes = {
    'IND': 'India',
    'BRA': 'Brazil',
    'CHN': 'China',
    'FRA': 'France',
    'JPN': 'Japan',
    'ESP': 'Spain',
    'GER': 'Germany',
    'SVK': 'Slovakia',
    'BEL': 'Belgium',
    'USA': 'United States',
    'AUS': 'Australia',
    'ARG': 'Argentina',
    'GBR': 'United Kingdom',
    'HUN': 'Hungary',
    'IDN': 'Indonesia',
    'RUS': 'Russia',
    'KAZ': 'Kazakhstan',
    'CZE': 'Czech Republic',
    'NGA': 'Nigeria',
    'SWE': 'Sweden',
    'NLD': 'Netherlands',
    'PRT': 'Portugal',
    'POL': 'Poland',
    'MEX': 'Mexico',
    'ITA': 'Italy',
    'BIH': 'Bosnia-Herzegovina',
    'UKR': 'Ukraine',
    'AUT': 'Austria',
    'TWN': 'Taiwan',
    'ZAF': 'South Africa'
}

def _get_us_file_factories(fname, source_name):
    with open(os.path.join(os.path.dirname(__file__), 'scrapped', fname), 'r') as inf:
        reader = csv.reader(inf)
        #header
        reader.next()
        for row in reader:
            if row:
                print "R", row
                address = row[1].strip()
                country = 'United States'
                parsed = geo_lookup.lat_lon_lookup('%s, USA' % (address), cache_file='facilities', force_google=True)
                #just ignore anything that's even slightly weird
                if parsed is not None and 'state' in parsed and 'city' in parsed:
                    state = parsed['state']
                    if country not in state_cache:
                        state_cache[country] = {}
                    if state != 'CA' and state in state_cache[country]:
                        continue #already have a plant in this region
                    else:
                        lat, lon = (parsed['lat'], parsed['lng'])
                        state_cache[country][state] = (lat, lon)
                    city = parsed['city']
                    writerow = [source_name, city, state, country, lat, lon]
                    yield writerow


state_cache = {}

with open('clean_plants.csv', 'wb') as csvfile:
    writer = csv.writer(csvfile, delimiter=',', quoting=csv.QUOTE_MINIMAL)
    writer.writerow(["True Source", "City", "State", "Country", "FactoryLat", "FactoryLon"])
    random.seed(999)


    with open(os.path.join(os.path.dirname(__file__), 'scrapped', 'vw_plants.csv'), 'r') as inf:
        reader = csv.reader(inf)
        #header
        reader.next()
        #VW is huge, skip ~75% of the plants
        for row in reader:
            if random.random() < 0.75:
                continue
            if row and len(row)>3:
                country = row[2].replace(",<br />",", ").replace("{{fb|","").replace("}}","")
                ioc_country = country[-3:]
                city = row[3]
                parts = city.split('<br />')
                parts = [pp.split('|')[-1] for pp in parts]
                city = ", ".join(parts).replace(',,',',').replace('-, ','-')
                parsed = geo_lookup.lat_lon_lookup('%s, %s' % (city, country), cache_file='facilities', force_google=True)
                if 'state' in parsed:
                    print parsed
                    print codes[ioc_country], "||", "||", city, "||", parsed['state']
                    state = parsed['state']
                    country = codes[ioc_country]
                    if country not in state_cache:
                        state_cache[country] = {}
                    row[2] = codes[ioc_country]
                    row[3] = city
                    state = unicodedata.normalize('NFKD', state).encode('ascii','ignore') #replace vin
                    if state in state_cache[country]:
                        continue #already have a plant in this region
                    else:
                        lat, lon = (parsed['lat'], parsed['lng'])
                        state_cache[country][state] = (lat, lon)
                    writerow = ["VW", city, state, codes[ioc_country], lat, lon]
                    writer.writerow(writerow)

    with open(os.path.join(os.path.dirname(__file__), 'scrapped', 'ford_plants.csv'), 'r') as inf:
        reader = csv.reader(inf)
        #header
        reader.next()
        for row in reader:
            if row:
                #Ford is also pretty big, skip ~50% of them
                if random.random() < 0.5:
                    continue
                city_state = row[2].strip()
                country = row[3].strip()
                parsed = geo_lookup.lat_lon_lookup('%s, %s' % (city_state, country), cache_file='facilities', force_google=True)
                #just ignore anything that's even slightly weird
                if parsed is not None and 'state' in parsed and 'city' in parsed:
                    state = parsed['state']
                    country = parsed['country']
                    if country not in state_cache:
                        state_cache[country] = {}
                    state = unicodedata.normalize('NFKD', state).encode('ascii','ignore') #replace vin
                    if state in state_cache[country]:
                        #already have a datapoint for this "region"
                        continue
                    else:
                        lat, lon = (parsed['lat'], parsed['lng'])
                        state_cache[country][state] = (lat, lon)
                    city = parsed['city']
                    city = unicodedata.normalize('NFKD', city).encode('ascii','ignore') #replace vin
                    writerow = ["Ford", city, state, country, lat, lon]
                    writer.writerow(writerow)

    with open(os.path.join(os.path.dirname(__file__), 'scrapped', 'cocacola_plants.csv'), 'r') as inf:
        reader = csv.reader(inf)
        #header
        reader.next()
        for row in reader:
            if row:
                city_state = row[1].strip()
                state = row[2].strip()
                country = 'United States'
                parsed = geo_lookup.lat_lon_lookup('%s, %s, %s' % (city, state, country), cache_file='facilities', force_google=True)
                #just ignore anything that's even slightly weird
                if parsed is not None and 'state' in parsed and 'city' in parsed:
                    state = parsed['state']
                    if country not in state_cache:
                        state_cache[country] = {}
                    if state in state_cache[country]:
                        continue #already have a plant in this region
                    else:
                        lat, lon = (parsed['lat'], parsed['lng'])
                        state_cache[country][state] = (lat, lon)
                    city = parsed['city']
                    writerow = ["Coke", city, state, country, lat, lon]
                    writer.writerow(writerow)

    for writerow in _get_us_file_factories('budweiser_plants.csv', 'Budweiser'):
        writer.writerow(writerow)

    for writerow in _get_us_file_factories('boeing_plants.csv', 'Boeing'):
        writer.writerow(writerow)

