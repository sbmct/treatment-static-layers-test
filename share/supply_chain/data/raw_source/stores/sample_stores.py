"""stores generator

Usage:
  sample_stores.py [-r sample_frequency]

Options:
  -r FLOAT   take this portion of the stores (defaults to 0.5)
"""
from docopt import docopt

import csv, random
import os.path
import itertools
import json


MAGIC_STATES = ('CA', 'OR', 'NV')

COMMON_FIELDS = [
    'Store Ident', #added by this script
    'Store Type', #added by this script
    'Address',
    'City',
    'Latitude',
    'Longitude',
    'Phone Number',
    'State',
    'Store Hours',
    'Store Number',
    'Zip Code',
]

IGNORE_ATTRS = ['Target Mobile']

def sample_stores(rate=0.5):
    store_types = ('store', 'superstore', 'market', 'express')
    counter = itertools.count()
    for store_type in store_types:
        fp = open(os.path.join(os.path.dirname(__file__), '%s.csv' % store_type))
        csvreader = csv.DictReader(fp)
        for store in csvreader:
            if random.random() < rate or store['State'] in MAGIC_STATES:
                #this store overlaps in the zoomed in region so ignore it
                if store_type != 'store' or store['Store Number'] != '3047':
                    #HACK there is no store at the locaiton we zoom into so add one
                    if store_type == 'superstore' and store['Store Number'] == '6672':
                        #This store is the store we zoom into - move the store icon to the center of the store
                        store['Latitude'] = 37.35431731148243
                        store['Longitude'] = -121.82439255699923
                    #set some info
                    store_number = counter.next()
                    store['Store Ident'] = 'franciscos-%d' % store_number
                    store['Store Number'] = store_number
                    store['Store Type'] = store_type
                    misc_info = {}
                    #make sure all the data stores have the same fields, so merge extra stuff into misc_info
                    for misc_field in store.keys():
                        if misc_field not in COMMON_FIELDS and misc_field not in IGNORE_ATTRS:
                            new_field = misc_field.replace('nUmber', 'Number') #fix common typo
                            misc_info[new_field] = str(store[misc_field])
                            del store[misc_field]
                    store['Misc Info'] = json.dumps(misc_info)
                    record = [store[f] for f in itertools.chain(COMMON_FIELDS, ['Misc Info'])]
                    yield record


def main():
    opts = docopt(__doc__)
    if opts['-r'] is None:
        sample_freq = 0.5
    else:
        sample_freq = float(opts['-r'])

    random.seed(0)
    with open(os.path.join(os.path.dirname(__file__), 'all_stores.csv'), 'w') as csvfile:
        writer = csv.writer(csvfile, delimiter=',', quoting=csv.QUOTE_MINIMAL)
        writer.writerow(COMMON_FIELDS + ['Misc Info'])
        for store in sample_stores(sample_freq):
            writer.writerow(store)

if '__main__' == __name__:
    main()

