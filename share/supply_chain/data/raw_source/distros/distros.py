import re
import csv
from sluice.geocode import geo_lookup
import os.path


with open('clean_distros.csv', 'w') as csvfile:
    writer = csv.writer(csvfile, delimiter=',', quoting=csv.QUOTE_MINIMAL)
    writer.writerow(["name", "address", "city", "state", "phone", "lat", "lon"])

    with open(os.path.join(os.path.dirname(__file__), 'raw_distros.txt'), 'r') as raw_data:
        previous_empty = True
        line_set = []
        for dirty_line in raw_data:
            line = dirty_line.strip()
            is_empty = not bool(len(line))
            if is_empty:
                if len(line_set) > 1:
                    name = line_set[0]
                    if not re.match('^[0-9\s\-\(\)]+$', line_set[-1]) or line_set[-1] == '()':
                        phone = '-'
                        address = line_set[1:]
                    else:
                        phone = line_set[-1]
                        address = line_set[1:-1]
                    parsed = geo_lookup.lat_lon_lookup('%s, USA' % (", ".join(address)), cache_file='distros', force_google=True)
                    #HACKS to work with google data
                    if address[-1] == 'Mira Loma, CA 91752':
                        parsed['city'] = 'Mira Loma'
                    elif address[-1] == 'Henderson, NC 27537':
                        parsed['city'] = 'Henderson'
                    if not parsed['address']:
                        #a couple don't have this, just add the address used to query
                        parsed['address'] = address[0]
                    writer.writerow([name, parsed['address'], parsed['city'], parsed['state'], phone, parsed['lat'], parsed['lng']])

                line_set = []
            else:
                line_set.append(line)


