Object.clone = function(obj, deep) {
    if(obj == null) { return null }
    if(typeof obj == 'string') { return obj+'' }
    if(typeof obj == 'number') { return obj+0 }
    if(typeof obj == 'boolean') { return obj }
    if(obj instanceof Array) {
        var n = obj.length;
        var dest = new Array(n);
        if(deep) {
            for(var i = 0; i < n; i++) {
                dest[i] = Object.clone(obj[i], true);
            }
        } else {
            for(var i = 0; i < n; i++) {
                dest[i] = obj[i];
            }
        }
        return dest;
    }
    var dest = { };
    if(deep) {
        for(var k in obj) {
            dest[k] = Object.clone(obj[k], true);
        }
    } else {
        for(var k in obj) {
            dest[k] = obj[k];
        }
    }
    return dest;
};

Array.prototype.collect = function(iter) {
    var n = this.length;
    var c = new Array(n);
    for(var i = 0; i < n; i++) {
        c[i] = iter(this[i], i);
    }
    return c;
};

Array.prototype.each = function(iter) {
    var n = this.length;
    for(var i = 0; i < n; i++) {
        iter(this[i], i);
    }
};

Array.prototype.every = function(iter) {
    var n = this.length;
    for(var i = 0; i < n; i++) {
        if(!iter(this[i], i)) {
            return false;
        }
    }
    return true;
};

Array.prototype.find = function(iter) {
    var n = this.length;
    for(var i = 0; i < n; i++) {
        if(iter(this[i], i)) {
            return this[i];
        }
    }
    return undefined;
};

Array.prototype.findAll = function(iter) {
    var m = [];
    var n = this.length;
    for(var i = 0; i < n; i++) {
        try {
        if(iter(this[i], i)) {
            m.push(this[i]);
        }
        } catch(e) {
            console.error('%o in iterator for element %o: %o', e, i, this[i]);
        }
    }
    return m;
};

Array.prototype.pull = function(iter) {
    var n = this.length;
    for(var i = 0; i < n; i++) {
        if(iter(this[i], i)) {
            var retval = this[i];
            this.splice(i, 1);
            return retval;
        }
    }
    return undefined;
};

function $A(iterable) {
    if(!iterable) return [];
    if('toArray' in Object(iterable)) return iterable.toArray();
    var length = iterable.length || 0, results = new Array(length);
    while (length--) results[length] = iterable[length];
    return results;
}

Element.create = function(tag, attrs) {
    var elem = document.createElement(tag);
    for(var k in attrs) {
        elem.setAttribute(k, attrs[k]);
    }
    return elem;
};

Element.prototype.update = function(elem) {
    if(elem == null) {
        this.innerHTML = '';
    } else if(typeof elem == 'string' || typeof elem == 'number') {
        this.innerHTML = elem;
    } else {
        this.innerHTML = elem.outerHTML;
    }
    return this;
};

Element.prototype.insert = function(elem) {
    if(elem instanceof Element) {
        this.appendChild(elem);
        return elem;
    } else if(elem.before) {
        this.insertBefore(elem.before);
        return elem.before;
    } else if(elem.after) {
        if(this.nextSibling) {
            this.nextSibling.insertBefore(elem.after);
        } else {
            this.parentNode.appendChild(elem.after);
        }
        return elem.after;
    } else if(elem.top) {
        this.parentNode.firstChild.insertBefore(elem.top);
        return elem.top;
    } else if(elem.bottom) {
        this.appendChild(elem.bottom);
        return elem.bottom;
    }
};


