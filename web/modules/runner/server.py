from flask import Blueprint, render_template, jsonify
from emcs_admin_utils import jsplasma
import plasma
import plasma.exceptions

runner = Blueprint('runner', __name__,
                   static_folder = 'static',
                   template_folder = 'templates')

@runner.route('/')
def base():
    return render_template('runner.html', jsplasma=jsplasma)

@runner.route('/fluoros')
def get_fluoros():
    resp_hose = plasma.Hose.participate('tcp://localhost/sluice-to-edge')
    req_hose = plasma.Hose.participate('tcp://localhost/edge-to-sluice')
    req_hose.deposit(plasma.Protein(['sluice', 'prot-spec v1.0', 'request', 'fluoroscopes'], {}))
    try:
        r = resp_hose.await_probe_frwd(['psa', 'fluoroscopes'], timeout=1)
        if r is None:
            raise plasma.exceptions.PoolAwaitTimedoutException
        info = r.ingests().to_json(True)
        return jsonify(**info)
    except plasma.exceptions.PoolAwaitTimedoutException:
        return 'Timed out waiting for list', 500

